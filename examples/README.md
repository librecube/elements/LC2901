# Examples

Here you can find examples of how to set up a SpaceCAN network. The examples
use the CAN module of the pyboard. If you want to use other microcontrollers
running MicroPython, you may need to add an external CAN module to them and
configure the device accordingly.

There are three examples that you can try out. Each example consists of a
controller node and two responder nodes (with id 1 and 2). Of course you can add more
nodes easily.

- **basic**: This example shows the basic SpaceCAN functionalities.
- **packet**: In this example, the telecommand/telemetry is replaced with the packet protocol.
- **services**: This example shows the packet utilization protocol (PUS) in action. The PUS is an extension of the above packet protocol.

## Set up the CAN network

For a minimal setup you will need two pyboards, which will be used as controller
node and responder node. To extend the network, simply add more responder nodes.

The hardware setup of controller and responder nodes is identical. They only differ
in the software setup, as described in the following.

In order for the nodes to communicate, each pyboard must be connected with
a CAN transceiver. The CAN transceivers then interface to the shared CAN bus.

A two-node setup with redundant CAN bus looks like this:

![image](minimal_setup.png)

To make the setup simpler, you may want to use a [CAN transceiver skin](https://gitlab.com/librecube/tools/pyboard-can-transceiver-skin) for the pyboard.

## Run the examples

1. Copy the `spacecan` folder from the `src` folder to each pyboard.

2. Copy the `config` folder to each pyboard.

3. For the controller node, copy the `controller.py` file to the pyboard. To have it start automatically at each boot, either rename it to `main.py` or create an empty `main.py` and put
the following statement in it: `import controller`.

4. For the responder node 1, copy the `responder1.py` file to the pyboard. To have it start automatically at each boot, either rename it to `main.py` or create an empty `main.py` and put
the following statement in it: `import responder1`.

And so on for additional nodes.
