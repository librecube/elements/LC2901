import time
import pyb
import micropython
import spacecan

micropython.alloc_emergency_exception_buf(100)
led_heartbeat = pyb.LED(1)


def received_sync():
    print("sync received")


def received_scet(coarse_time, fine_time):
    print(f"scet received [{coarse_time, fine_time}]")


def received_utc(day, ms_of_day, sub_ms):
    print(f"utc received [{day, ms_of_day, sub_ms}]")


def received_packet(packet_data, node_id):
    print(f"packet received [{packet_data.hex()}]")


def on_bus_switch():
    print("bus switched")


responder = spacecan.Responder.from_file("config/responder2.json")
responder.received_heartbeat = led_heartbeat.toggle
# responder.received_sync = received_sync
responder.received_scet = received_scet
responder.received_utc = received_utc
responder.received_packet = received_packet
responder.on_bus_switch = on_bus_switch
responder.connect()
responder.start()

try:
    while True:
        time.sleep(5)

        # send dummy packet
        data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
        packet = spacecan.Packet(data)
        responder.send_packet(packet)

except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
